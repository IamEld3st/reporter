"""Test mocks."""
from unittest.mock import MagicMock


def get_mocked_checkout(valid=True, builds=[], issues=[]):
    """Return a kcidb checkout mock."""
    checkout = MagicMock()
    checkout.valid = valid
    checkout.issues.list.return_value = issues

    checkout.builds.list.return_value = builds
    return checkout


def get_mocked_build(valid=True, tests=[], issues=[]):
    """Return a kcidb build mock."""
    build = MagicMock()
    build.valid = valid
    build.issues.list.return_value = issues

    build.tests.list.return_value = tests
    return build


def get_mocked_test(status='PASSED', waived=False, issues=[]):
    """Return a kcidb test mock."""
    test = MagicMock()
    test.status = status
    test.waived = waived
    test.issues.list.return_value = issues

    return test
