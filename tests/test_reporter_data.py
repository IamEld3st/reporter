from unittest import TestCase
from unittest.mock import MagicMock
from unittest.mock import patch

from reporter.data import BuildData
from reporter.data import CheckoutData
from reporter.data import TestData

from .mocks import get_mocked_build
from .mocks import get_mocked_checkout
from .mocks import get_mocked_test

TestData.__test__ = False  # To supress a pytest warning


class TestReporterData(TestCase):
    """Tests for reporter/data.py ."""

    @patch('reporter.data.urlopen')
    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data(self, checkouts_get_mock, urlopen_mock):
        """Test the CheckoutData class and it's properties."""
        tests = [get_mocked_test('PASS', False)]
        builds = [get_mocked_build(True, tests)]
        issues = ['Issue']
        checkout = get_mocked_checkout(True, builds, issues)
        checkout.misc = {'nvr': 'n-v-r'}

        checkout_id = '123'
        checkouts_get_mock.return_value = checkout
        checkout_data = CheckoutData(checkout_id)
        checkouts_get_mock.assert_called_with(id=checkout_id)

        self.assertEqual(checkout_data.checkout, checkout)
        self.assertEqual(checkout_data.nvr, 'n-v-r')

        merge_log = MagicMock()
        merge_log.read.side_effect = [b'bar']
        urlopen_mock.return_value.__enter__.return_value = merge_log

        self.assertEqual(checkout_data.mergelog,
                         'bar')

        self.assertEqual(checkout_data.issues, issues)

        self.assertEqual(checkout_data.build_data.builds, builds)
        self.assertEqual(checkout_data.test_data.tests, tests)

        self.assertTrue(checkout_data.result)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_failed_build_result(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests),
                  get_mocked_build(False, tests, ['Issue'])]
        checkout = get_mocked_checkout(True, builds)

        checkouts_get_mock.return_value = checkout
        checkout_data = CheckoutData('1234')

        self.assertFalse(checkout_data.result)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_known_issue_build_result(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests, ['Issue'])]
        checkout = get_mocked_checkout(True, builds)

        checkouts_get_mock.return_value = checkout
        checkout_data = CheckoutData('1234')

        self.assertTrue(checkout_data.result)

    def test_build_data(self):
        """Test the BuildData class and it's properties."""
        tests = []
        passed_build = get_mocked_build(True, tests)
        failed_build = get_mocked_build(False, tests)
        known_failure_build = get_mocked_build(False, tests, ['Issue'])

        builds = MagicMock()
        builds.list.return_value = [passed_build, failed_build,
                                    known_failure_build]

        build_data = BuildData(builds)

        self.assertEqual(build_data.passed_builds, [passed_build])
        self.assertEqual(build_data.failed_builds,
                         [failed_build, known_failure_build])
        self.assertEqual(build_data.known_issues_builds,
                         [known_failure_build])
        self.assertEqual(build_data.unknown_issues_builds, [failed_build])

    def test_test_data(self):
        """Test the TestData class and it's properties."""
        pass_test = get_mocked_test('PASS', False)
        pass_waived_test = get_mocked_test('PASS', True)
        fail_test = get_mocked_test('FAIL', False)
        fail_waived_test = get_mocked_test('FAIL', True)
        error_test = get_mocked_test('ERROR', False)
        skip_test = get_mocked_test('SKIP', False)
        none_test = get_mocked_test(None, False)
        fail_known_test = get_mocked_test('FAIL', False, ['issue'])
        tests = [pass_test, fail_test, pass_waived_test, fail_waived_test,
                 error_test, skip_test, none_test, fail_known_test]

        builds = MagicMock()
        builds.list.return_value = [get_mocked_build(True, tests)]

        build_data = BuildData(builds)
        test_data = TestData(build_data)

        self.assertEqual(test_data.tests, tests)

        self.assertEqual(test_data.failed_tests,
                         [fail_test, fail_known_test])
        self.assertEqual(test_data.passed_tests,
                         [pass_test, pass_waived_test])
        self.assertEqual(test_data.skipped_tests, [skip_test, none_test])
        self.assertEqual(test_data.non_skipped_tests,
                         [test for test in tests
                          if test not in test_data.skipped_tests])
        self.assertEqual(test_data.errored_tests, [error_test])
        self.assertEqual(test_data.waived_tests,
                         [pass_waived_test, fail_waived_test])
        self.assertEqual(test_data.known_issues_tests, [fail_known_test])
        self.assertEqual(test_data.unknown_issues_tests,
                         [fail_test])  # I'm not expecting the waived test here
