from argparse import Namespace
import os
from unittest import TestCase
from unittest.mock import patch

patch.dict(os.environ, {'RETRY_COUNT': '2', 'RETRY_DELAY': '1'}).start()


class TestMain(TestCase):
    """Tests for reporter/__main__.py ."""

    default_args = Namespace(listen=False, checkout_id=None, template=None)
    listen_args = Namespace(listen=True, checkout_id=None, template=None)
    checkout_args = Namespace(listen=False, checkout_id='asdf123',
                              template=None)
    template_args = Namespace(listen=False, checkout_id='asdf123',
                              template='long_report.j2')

    @patch('reporter.__main__.render_template')
    def test_process_message(self, render_template_mock):
        """Test the process_message function."""
        from reporter.__main__ import ReporterException
        from reporter.__main__ import process_message

        expected_data = {
            'timestamp': '2020-07-28T14:51:09.845612+00:00',
            'status': 'ready_to_report',
            'object_type': 'checkout',
            'id': '57255914550bf453388c489390ed2f5da0035555'
        }

        process_message("", expected_data)

        render_template_mock.assert_called_with(expected_data['id'])
        self.assertEqual(render_template_mock.call_count, 1)

        # Call with an unexpected object_type
        with self.assertRaises(ReporterException):
            expected_data['object_type'] = 'foo'
            process_message("", expected_data)
        self.assertEqual(render_template_mock.call_count, 1)

        # Call again with a status that should be ignored
        expected_data['status'] = 'new'
        process_message("", expected_data)

        self.assertEqual(render_template_mock.call_count, 1)

    def test_create_parser(self):
        """Test the create_parser function."""
        from reporter.__main__ import create_parser

        parser = create_parser()

        args = parser.parse_args([])
        self.assertEqual(args, self.default_args)

        args = parser.parse_args(['-l'])
        self.assertEqual(args, self.listen_args)

        args = parser.parse_args(['-c', self.checkout_args.checkout_id])
        self.assertEqual(args, self.checkout_args)

        with self.assertRaises(SystemExit):
            parser.parse_args(['-l', '-c', self.checkout_args.checkout_id])

    @patch('cki_lib.misc.is_production')
    @patch('sentry_sdk.init')
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.settings.QUEUE.consume_messages')
    def test_main_default(self, consume_messages_mock, args_mock,
                          sentry_init_mock, is_production_mock):
        """Test main with no arguments."""
        from reporter.__main__ import main

        args_mock.return_value = self.default_args
        is_production_mock.return_value = True
        main()
        assert consume_messages_mock.called
        assert sentry_init_mock.called

    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.settings.QUEUE.consume_messages')
    def test_main_listen(self, consume_messages_mock, args_mock):
        """Test main with listen argument."""
        from reporter.__main__ import main

        args_mock.return_value = self.listen_args
        main()
        assert consume_messages_mock.called

    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.__main__.CheckoutData')
    def test_main_checkout_id(self, checkout_data_mock, args_mock):
        """Test main with checkout id specified."""
        from reporter.__main__ import main

        args_mock.return_value = self.checkout_args
        with self.assertRaises(SystemExit):
            main()
        checkout_data_mock.assert_called_once_with(
            self.checkout_args.checkout_id)

    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.settings.JINJA_ENV.get_template')
    @patch('reporter.__main__.CheckoutData')
    def test_main_template(self, checkout_data_mock, get_template_mock,
                           args_mock):
        """Test main with checkout id specified."""
        from reporter.__main__ import main

        args_mock.return_value = self.template_args
        with self.assertRaises(SystemExit):
            main()
        checkout_data_mock.assert_called_once_with(
            self.template_args.checkout_id)
        get_template_mock.assert_called_once_with(
            self.template_args.template)

    @patch('argparse.ArgumentParser.parse_args')
    @patch('cki_lib.misc.is_production')
    @patch('reporter.__main__.CheckoutData')
    def test_main_exit(self, checkout_data_mock, is_production_mock,
                       args_mock):
        """Test main with checkout id specified."""
        from reporter.__main__ import main

        args_mock.return_value = self.template_args
        is_production_mock.return_value = True
        main()

        is_production_mock.return_value = False
        checkout_data_mock.return_value.result = True
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 0)

        checkout_data_mock.return_value.result = False
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 1)

    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.__main__.CheckoutData')
    def test_unknown_checkout_retry_checkout(self, checkout_data_mock, args_mock):
        """Test the retry system with a single checkout run."""
        from reporter.__main__ import main

        args_mock.return_value = self.checkout_args
        not_found = Exception(b'{"detail":"Not found."}')
        checkout_data_mock.side_effect = not_found

        with self.assertRaises(Exception) as exception:
            main()

        self.assertEqual(exception.exception, not_found)
        self.assertEqual(len(checkout_data_mock.mock_calls), 2)

    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.__main__.CheckoutData')
    def test_unknown_checkout_retry_process_message(self, checkout_data_mock,
                                                    args_mock):
        """Test the retry system with listening."""
        from reporter.__main__ import process_message

        not_found = Exception(b'{"detail":"Not found."}')
        checkout_data_mock.side_effect = not_found
        payload = {'status': 'ready_to_report', 'object_type': 'checkout',
                   'id': 'asdf123'}

        with self.assertRaises(Exception) as exception:
            process_message(None, payload)

        self.assertEqual(exception.exception, not_found)
